package com.someguy.grifiti.modules

import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("db")
class MongoDbConfiguration : AbstractMongoConfiguration() {

    lateinit var connectionUri : MongoClientURI

    override fun mongoClient(): MongoClient {
        val client = MongoClient(connectionUri)
        return client
    }

    override fun getDatabaseName() = "sprays"
}
