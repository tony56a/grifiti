package com.someguy.grifiti

import com.someguy.grifiti.services.SprayService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@SpringBootApplication
@EnableMongoRepositories
class HelloProfileApplication {

    @Autowired
    lateinit var sprayService: SprayService
}


fun main(args: Array<String>) {
    runApplication<HelloProfileApplication>(*args)
}

