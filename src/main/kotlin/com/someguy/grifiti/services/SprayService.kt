package com.someguy.grifiti.services

import com.google.protobuf.Empty
import com.someguy.grifiti.entities.LocationEntity
import com.someguy.grifiti.entities.SprayEntity
import com.someguy.grifiti.handlers.SprayHandler
import com.someguy.grifiti.spray.GrifitiServer
import com.someguy.grifiti.spray.SprayServiceGrpc
import io.grpc.stub.StreamObserver
import org.apache.logging.log4j.kotlin.Logging
import org.lognet.springboot.grpc.GRpcService
import org.springframework.beans.factory.annotation.Autowired
import java.time.Instant
import java.util.*

@GRpcService
class SprayService @Autowired constructor(
        private val sprayHandler: SprayHandler
) : SprayServiceGrpc.SprayServiceImplBase(), Logging {

    override fun createSpray(request: GrifitiServer.CreateSprayRequest,
                             responseObserver: StreamObserver<GrifitiServer.CreateSprayResponse>) {
        val requestSpray = request.sprayInput ?: throw IllegalArgumentException("Invalid SprayEntity")
        logger.info("Creating thing ${requestSpray.anchorId}")
        val sprayId = UUID.randomUUID()
        this.sprayHandler.saveSpray(SprayEntity.fromSpray(requestSpray, sprayId))
        responseObserver.onNext(GrifitiServer.CreateSprayResponse.newBuilder().setSprayId(sprayId.toString()).build())
        responseObserver.onCompleted()
    }

    override fun retrieveSpray(request: GrifitiServer.RetrieveSprayRequest,
                               responseObserver: StreamObserver<GrifitiServer.RetrieveSprayResponse>) {
        val id = UUID.fromString(request.sprayId ?: throw IllegalArgumentException("Spray ID not found"))
        logger.info("Retrieving thing $id")
        val sprayEntity = this.sprayHandler.getSpray(id)
        val retrieveSprayResponse = GrifitiServer.RetrieveSprayResponse.newBuilder()
        sprayEntity?.let { spray ->
            retrieveSprayResponse.setSpray(SprayEntity.toSpray(spray))
        }
        responseObserver.onNext(retrieveSprayResponse.build())
        responseObserver.onCompleted()
    }

    override fun querySpray(request: GrifitiServer.QuerySprayRequest, responseObserver: StreamObserver<GrifitiServer.QuerySprayResponse>) {
        val location = request.location ?: throw IllegalArgumentException("Location not found")
        val range = request.range
        if (range <= 0) {
            throw IllegalArgumentException("Range not valid")
        }
        logger.info("Querying sprays at $location, with a range of ${range}")
        val sprays = this.sprayHandler.querySprayByLocation(LocationEntity(location.longitude, location.latitude), 30.0, 0).map { spray -> SprayEntity.toSpray(spray) }
        val querySprayResponse = GrifitiServer.QuerySprayResponse.newBuilder()
        querySprayResponse.addAllSprays(sprays)
        responseObserver.onNext(querySprayResponse.build())
        responseObserver.onCompleted()
    }

    override fun deleteSpray(request: GrifitiServer.DeleteSprayRequest, responseObserver: StreamObserver<GrifitiServer.DeleteSprayResponse>) {
        val id = UUID.fromString(request.sprayId ?: throw IllegalArgumentException("Spray ID not found"))
        logger.info("Deleting thing $id")
        val deletedId = this.sprayHandler.deleteSpray(id)
        responseObserver.onNext(GrifitiServer.DeleteSprayResponse.newBuilder().setSprayId(deletedId.toString()).build())
        responseObserver.onCompleted()
    }

    override fun ping(request: Empty, responseObserver: StreamObserver<GrifitiServer.PingResponse>) {
        logger.info("Pinged at ${Instant.now().toEpochMilli()}")
        responseObserver.onNext(GrifitiServer.PingResponse.newBuilder().setStatus("OK").build())
        responseObserver.onCompleted()
    }

}