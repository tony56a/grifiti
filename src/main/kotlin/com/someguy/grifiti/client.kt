package com.someguy.grifiti

import com.google.protobuf.Timestamp
import com.someguy.grifiti.spray.GrifitiServer
import com.someguy.grifiti.spray.SprayServiceGrpc
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.StatusRuntimeException
import java.time.Instant
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger

/**
 * A simple client that requests a greeting from the [HelloWorldServer].
 */
class HelloWorldClient
/** Construct client for accessing RouteGuide server using the existing channel.  */
internal constructor(private val channel: ManagedChannel) {
    private val blockingStub: SprayServiceGrpc.SprayServiceBlockingStub
            = SprayServiceGrpc.newBlockingStub(channel)

    /** Construct client connecting to HelloWorld server at `host:port`.  */
    constructor(host: String, port: Int) : this(ManagedChannelBuilder.forAddress(host, port)
            // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
            // needing certificates.
            .usePlaintext()
            .build())


    @Throws(InterruptedException::class)
    fun shutdown() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }

    /** Say hello to server.  */
    fun greet(name: String, port: Int = 50051) {
        logger.log(Level.INFO, "Will try to create a thing with ${name} on port $port")
        val instant = Instant.now()
        val thing = GrifitiServer.Spray.newBuilder()
                .setAnchorId("aaa")
                .setName(name)
                .setLocation(GrifitiServer.Point.newBuilder().setLongitude(43.641461).setLatitude(-79.391291).build())
                .setCreationTime(Timestamp.newBuilder().setSeconds(instant.epochSecond).setNanos(instant.nano).build())
                .setSprayType(GrifitiServer.SprayType.IMAGE)
                .setResourceUrl("aaa")
                .build()

        val request = GrifitiServer.CreateSprayRequest.newBuilder().setSprayInput(thing).build()
        val response: GrifitiServer.CreateSprayResponse =  try {
            blockingStub.createSpray(request)
        } catch (e: StatusRuntimeException) {
            logger.log(Level.WARNING, "RPC failed: ${e.status}")
            return
        }

        logger.info("Retrieving a thing: ${response.sprayId}")

        val retrieveRequest = GrifitiServer.RetrieveSprayRequest.newBuilder().setSprayId(response.sprayId).build()
        val retrieveResponse: GrifitiServer.RetrieveSprayResponse =  try {
            blockingStub.retrieveSpray(retrieveRequest)
        } catch (e: StatusRuntimeException) {
            logger.log(Level.WARNING, "RPC failed: ${e.status}")
            return
        }

        logger.info("Got a thing: ${retrieveResponse.spray}")

        val queryRequest = GrifitiServer.QuerySprayRequest.newBuilder()
                .setLocation(GrifitiServer.Point.newBuilder().setLongitude(43.641461).setLatitude(-79.391291).build())
                .setRange(30.0)
                .build()
        val queryResponse: GrifitiServer.QuerySprayResponse =  try {
            blockingStub.querySpray(queryRequest)
        } catch (e: StatusRuntimeException) {
            logger.log(Level.WARNING, "RPC failed: ${e.status}")
            return
        }

        logger.info("Got ${queryResponse.spraysCount} sprays")
    }

    companion object {
        private val logger = Logger.getLogger(HelloWorldClient::class.java.name)

        /**
         * Greet server. If provided, the first element of `args` is the name to use in the
         * greeting.
         */
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val portNum = 50051;
            val client = HelloWorldClient("localhost", portNum)
            try {
                /* Access a service running on the local machine on port 5000 */
                val user = if (args.isNotEmpty()) args[0] else "world"
                client.greet(user, portNum)
            } finally {
                client.shutdown()
            }
        }
    }
}