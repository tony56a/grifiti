package com.someguy.grifiti.entities

enum class SprayTypeEntity {
    IMAGE,
    VIDEO,
    MODEL
}