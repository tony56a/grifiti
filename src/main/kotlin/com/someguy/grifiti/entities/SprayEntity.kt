package com.someguy.grifiti.entities

import com.google.protobuf.Timestamp
import com.someguy.grifiti.spray.GrifitiServer
import com.someguy.grifiti.spray.GrifitiServer.Point
import com.someguy.grifiti.spray.GrifitiServer.Spray
import java.time.Instant
import java.util.*


data class SprayEntity(val id: UUID,
                       val name: String,
                       val anchorId: String,
                       val locationEntity: LocationEntity,
                       val creationTime: Instant,
                       val sprayType: SprayTypeEntity,
                       val resourceUrl: String) {
    companion object {
        fun toSpray(sprayEntity: SprayEntity) : Spray {
            return Spray.newBuilder()
                    .setId(sprayEntity.id.toString())
                    .setName(sprayEntity.name)
                    .setAnchorId(sprayEntity.anchorId)
                    .setLocation(Point.newBuilder()
                            .setLatitude(sprayEntity.locationEntity.latitude)
                            .setLongitude(sprayEntity.locationEntity.longitude)
                            .build())
                    .setCreationTime(Timestamp.newBuilder()
                            .setSeconds(sprayEntity.creationTime.epochSecond)
                            .setNanos(sprayEntity.creationTime.nano))
                    .setSprayType(when(sprayEntity.sprayType) {
                        SprayTypeEntity.IMAGE -> GrifitiServer.SprayType.IMAGE
                        SprayTypeEntity.VIDEO -> GrifitiServer.SprayType.VIDEO
                        SprayTypeEntity.MODEL -> GrifitiServer.SprayType.MODEL
                    })
                    .setResourceUrl(sprayEntity.resourceUrl)
                    .build()
        }

        fun fromSpray(spray : Spray, uuid: UUID): SprayEntity {
            return SprayEntity(
                    id = uuid,
                    name = spray.name,
                    anchorId = spray.anchorId,
                    locationEntity = LocationEntity(spray.location.longitude, spray.location.latitude),
                    creationTime = Instant.ofEpochSecond(spray.creationTime.seconds,
                            spray.creationTime.nanos.toLong()),
                    sprayType = when(spray.sprayType) {
                        GrifitiServer.SprayType.IMAGE -> SprayTypeEntity.IMAGE
                        GrifitiServer.SprayType.VIDEO -> SprayTypeEntity.VIDEO
                        GrifitiServer.SprayType.MODEL -> SprayTypeEntity.MODEL
                        else -> throw IllegalArgumentException("Invalid Spray Type")
                    },
                    resourceUrl = spray.resourceUrl
            )
        }
    }
}