package com.someguy.grifiti.entities

data class LocationEntity (val longitude: Double,
                           val latitude: Double)