package com.someguy.grifiti.models

import com.someguy.grifiti.entities.LocationEntity
import com.someguy.grifiti.entities.SprayEntity
import com.someguy.grifiti.entities.SprayTypeEntity
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.*

@TypeAlias("spray")
@Document
data class SprayModel(@Id val id: UUID,
                      val name: String = "Some Name",
                      val anchorId: String,
                      @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE) val location: GeoJsonPoint,
                      val creationTime : Long,
                      val sprayType: String,
                      val resourceUrl: String) {

    companion object {

        fun fromSprayEntity(sprayEntity: SprayEntity) : SprayModel {
            return SprayModel(
                    id = sprayEntity.id,
                    name = sprayEntity.name,
                    anchorId = sprayEntity.anchorId,
                    location = GeoJsonPoint(sprayEntity.locationEntity.longitude,
                            sprayEntity.locationEntity.latitude),
                    creationTime = sprayEntity.creationTime.toEpochMilli(),
                    sprayType = sprayEntity.sprayType.name,
                    resourceUrl = sprayEntity.resourceUrl)
        }

        fun toSprayEntity(sprayModel: SprayModel) : SprayEntity {
            return SprayEntity(
                    id = sprayModel.id,
                    name = sprayModel.name,
                    anchorId = sprayModel.anchorId,
                    locationEntity = LocationEntity(sprayModel.location.x, sprayModel.location.y),
                    creationTime = Instant.ofEpochMilli(sprayModel.creationTime),
                    sprayType = SprayTypeEntity.valueOf(sprayModel.sprayType),
                    resourceUrl = sprayModel.resourceUrl
            )
        }
    }
}
