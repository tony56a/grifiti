package com.someguy.grifiti.repositories

import com.someguy.grifiti.models.SprayModel
import org.springframework.data.domain.Pageable
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Point
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*


interface SprayRepository : MongoRepository<SprayModel, UUID> {

    fun findAllByLocationNear(p: Point, d: Distance, pageable: Pageable) : List<SprayModel>

}