package com.someguy.grifiti.handlers


import com.someguy.grifiti.entities.LocationEntity
import com.someguy.grifiti.entities.SprayEntity
import com.someguy.grifiti.models.SprayModel
import com.someguy.grifiti.repositories.SprayRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Metrics
import org.springframework.data.geo.Point
import org.springframework.stereotype.Component
import java.util.*

@Component
class SprayHandler @Autowired constructor(val sprayRepository: SprayRepository) {

    fun saveSpray(sprayEntity: SprayEntity) {
        val sprayModel = SprayModel.fromSprayEntity(sprayEntity)
        sprayRepository.save(sprayModel)
    }

    fun getSpray(id : UUID) : SprayEntity? {
        val sprayModel : SprayModel? = sprayRepository.findById(id).orElse(null)
        return sprayModel?.let{ model -> SprayModel.toSprayEntity(model) }
    }

    fun querySprayByLocation(locationEntity: LocationEntity, distance: Double = 30.0, page: Int = 0) : List<SprayEntity> {
        val sprays = sprayRepository.findAllByLocationNear(Point(locationEntity.longitude, locationEntity.latitude),
                Distance(distance / 1000.0 , Metrics.KILOMETERS),
                PageRequest.of(page, 1000))
        return sprays.map { sprayModel ->
            SprayModel.toSprayEntity(sprayModel)
        }
    }

    fun deleteSpray(id: UUID) : UUID {
        sprayRepository.deleteById(id)
        return id
    }
}