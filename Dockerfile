FROM gradle:jdk12 as java-builder

WORKDIR /gradle

COPY . .

RUN gradle bootjar

FROM golang:1.12-alpine as golang-builder

WORKDIR /go/src/github.com/someguy/grifiti
COPY gateway.go .
COPY src/main/proto/spray/spray.proto ./src/main/proto/spray/spray.proto

RUN apk add --no-cache git mercurial protobuf-dev build-base
RUN go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
RUN go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
RUN go get -u github.com/golang/protobuf/protoc-gen-go

RUN protoc -I/usr/local/include -I. \
  -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
  --go_out=plugins=grpc:. \
  src/main/proto/spray/spray.proto

RUN protoc -I/usr/local/include -I. \
    -I$GOPATH/src \
    -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    --grpc-gateway_out=logtostderr=true:. \
    src/main/proto/spray/spray.proto

RUN go get golang.org/x/net/context
RUN go get google.golang.org/grpc
RUN go get google.golang.org/grpc/codes
RUN go get google.golang.org/grpc/metadata
RUN go get google.golang.org/grpc/status

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-linkmode external -extldflags -static"\
                                                                            -a gateway.go
FROM adoptopenjdk/openjdk12:alpine-slim as executor

COPY --from=java-builder /gradle/build/libs/grifiti.jar /usr/app/grifiti.jar
COPY --from=java-builder /gradle/build/resources/main/application-heroku.yaml /usr/app/application.yaml
COPY --from=golang-builder /go/src/github.com/someguy/grifiti/gateway /usr/app/gateway
COPY executor.sh /usr/app/

EXPOSE 8080
EXPOSE 50051

CMD ["/usr/app/executor.sh"]
