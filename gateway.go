package main

import (
  "os"
  "fmt"
  "flag"
  "net/http"

  "github.com/golang/glog"
  "golang.org/x/net/context"
  "github.com/grpc-ecosystem/grpc-gateway/runtime"
  "google.golang.org/grpc"

  sprayGateway "./src/main/proto/spray"
)

var (
  echoEndpoint = flag.String("echo_endpoint", "localhost:50051", "endpoint of the SprayService")
)

func getenv(key, fallback string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return fallback
    }
    return value
}

func run() error {
  ctx := context.Background()
  ctx, cancel := context.WithCancel(ctx)
  defer cancel()

  mux := runtime.NewServeMux()
  opts := []grpc.DialOption{grpc.WithInsecure()}
  err := sprayGateway.RegisterSprayServiceHandlerFromEndpoint(ctx, mux, *echoEndpoint, opts)
  if err != nil {
    return err
  }

  portNum := getenv("PORT", "8080")
  port := fmt.Sprintf(":%s", portNum)

  return http.ListenAndServe(port, mux)
}

func main() {
  flag.Parse()
  defer glog.Flush()

  if err := run(); err != nil {
    glog.Fatal(err)
  }
}